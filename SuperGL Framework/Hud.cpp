#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include <vector>
#include <iostream>
#include "Hud.h"

Hud::Hud(std::string text, glm::vec4 color, glm::vec3 position, unsigned int fontBase, bool second_dim, float scale)
	:	m_text(text), m_color(color), m_position(position), m_fontBase(fontBase),
		m_2D(second_dim), m_scale(scale)
{ }

void Hud::render()
{
	if(m_2D)
	{
		glPushMatrix ();
		glLoadIdentity ();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix ();
		glLoadIdentity();
		GLint viewport [4];
		glGetIntegerv (GL_VIEWPORT, viewport);
		gluOrtho2D (0,viewport[2], viewport[3], 0);
		glDepthFunc (GL_ALWAYS);
	}
	
	glPushMatrix();
	glColor4f(m_color.r, m_color.g, m_color.b, m_color.a);
	glTranslatef(m_position.x, m_position.y, m_position.z);
	glScalef(m_scale,-m_scale, 50.0f);
	glPushAttrib(GL_LIST_BIT);
	glListBase(m_fontBase);
	glCallLists(m_text.length(), GL_UNSIGNED_BYTE, m_text.c_str());
	glPopAttrib();
	glPopMatrix();
	glFlush();

	if(m_2D)
	{
		glDepthFunc (GL_LESS);
		glPopMatrix ();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix ();
	}
}

Cube Hud::toCube() const
{
	glm::vec3 min_point = m_position;
	glm::vec3 max_point = m_position;
	max_point += m_text.length();

	return Cube(min_point, max_point);
}
