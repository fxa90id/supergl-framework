using namespace std;

#include "MeshObject.h"
#include "MeshManager.h"
#include "Logger.h"

MeshManager* MeshManager::getSingleton()
{
	static MeshManager mm;
	return &mm;
}
void MeshManager::add(string key, string path) {
	MeshObject * mobject = load_object(path.c_str());
	if(!mobject) {
		Logger log;
		log.addStream(&cout);
		log.beginMessage();
		log << "nie udalo sie wczytac modelu: " << path << endl;
		return;
	}

	// nie znaleziono obiektu
	if(get(key) == nullptr) {
		m_container.insert(std::make_pair(key, mobject));
	} else {
		Logger log;
		log.addStream(&cout);
		log.beginMessage();
		log << "Model o kluczu " << key << " juz istnieje!" << endl;
	}
}
void MeshManager::remove(string key) {
	auto it = m_container.find(key);
	if(it != m_container.end()) {
		delete it->second;
		m_container.erase(it);
	}
}
MeshObject* MeshManager::get(string key) {
	auto it = m_container.find(key);
	if(it != m_container.end()) {
		return it->second;
	}
	return nullptr;
}

MeshObject* MeshManager::load_object(const char* filename)
{
	ifstream objectFile(filename, ios::in);

	if(!objectFile) {
		cerr << "Otworzenie obiektu: " << filename << " nie powiodlo sie!";
		return nullptr;
	}
	vector<glm::vec3> vertices;
	vector<glm::vec3> normals;
	vector<GLushort> elements; 
	MeshObject* mesh = new MeshObject();
	string line;
	while(getline(objectFile, line)) {
		if(line.substr(0, 2) == "v ") {
			istringstream s(line.substr(2));
			glm::vec3 v;
			s >> v.x;
			s >> v.y;
			s >> v.z;
	// 		s >> (v.w = 1.0f);
			vertices.push_back(v);
		} else if(line.substr(0,2) == "f ") {
			istringstream s(line.substr(2));
			GLushort a,b,c,d;
			s >> a;
			s >> b;
			s >> c;
			s >> d;
			a--; b--; c--; d--;
			elements.push_back(a);
			elements.push_back(b);
			elements.push_back(c);
			elements.push_back(d);

		} else if(line[0] == '#') { 
			/* comment, ignoring */ 
			continue;
		} else {
			/* ignoring */
			continue;
		}

		normals.resize(vertices.size(), glm::vec3(0,0,0));
		unsigned int size = elements.size();
		GLushort ia,ib,ic,id;
		for(int i=0; i < size; i+=4) {
			ia = elements[i];
			ib = elements[i+1];
			ic = elements[i+2];
			id = elements[i+3];

			glm::vec3 normal = glm::normalize(glm::cross(
				glm::vec3(vertices[ib]) - glm::vec3(vertices[ia]),
				glm::vec3(vertices[ic]) - glm::vec3(vertices[ia])
			));

			normals[ia] = normals[ib] = normals[ic] = normals[id] = normal;
		} 
		/*
			tutaj powinni�my stworzy� VBO przekaza� go do mesha,
			oraz liczb� element�w.
		*/
	}

	// GLuint vbo = 0; // createVBO(data,  dataSize,  target,  usage);
	
	mesh->attach(vertices, normals, elements);
	return mesh;
}

/*
GLuint MeshManager::createVBO(const void * data, int dataSize, GLenum target, GLenum usage) {
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);

	GLuint vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	return vbo;
}
*/
