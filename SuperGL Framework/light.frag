varying vec3 normal,halfVector;
varying vec4 lightDir;

vec3 n,halfV;
vec4 color;
float NdotL,NdotHV;

void enableLightFrag()
{
	//color = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
	n = normalize(normal);
	NdotL = dot(vec4(n, 1.0),lightDir);

	if (NdotL > 0.0)
	{
		color = gl_LightSource[0].diffuse *
		gl_FrontMaterial.diffuse * NdotL;
		halfV = normalize(halfVector);
		NdotHV = max(dot(n,halfV),0.0);
	}
	
	color *= gl_LightSource[0].specular;
	gl_FragColor += 0.5 * color;
}