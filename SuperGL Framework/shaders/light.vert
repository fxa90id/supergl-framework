varying vec3 normal,lightDir,halfVector;

void enableLightVert()
{
	 normal = normalize(gl_NormalMatrix * gl_Normal);
	 lightDir = normalize(vec3(gl_LightSource[0].position) + vec3(4.0, 5.0, 2.0));
	 halfVector = normalize(gl_LightSource[0].halfVector.xyz);
}