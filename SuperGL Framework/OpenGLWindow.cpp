#include <iostream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <algorithm>
#include "OpenGLWindow.h"
#include "SphericalCamera.h"

using namespace SuperGLFramework;

OpenGLWindow::OpenGLWindow(std::string title, size_t width, size_t height, HINSTANCE instance)
	:	OpenGLWindow::Window(title, width, height, instance),
		m_pCamera(new SphericalCamera)
{ }

OpenGLWindow::~OpenGLWindow() 
{ 
	destroy();
}

void OpenGLWindow::create()
{
	Window::create();
	createContext();
	initGL();
	setVisible(true);
	resize(m_width, m_height);
	using namespace std::placeholders;
	addOnResize(std::bind(&OpenGLWindow::resize, this, _1, _2));
	addFunctionToLoop(std::bind(&OpenGLWindow::draw, this));
}

void OpenGLWindow::destroy() 
{
	Window::destroy();

	if(m_hRC) 
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_hRC);
		m_hRC = nullptr;
	}

	if(m_hDC) 
	{
		ReleaseDC(m_hWnd, m_hDC);
		m_hDC = nullptr;
	}
}

void OpenGLWindow::runEventLoop()
{
	Window::runEventLoop();
}

void OpenGLWindow::resize(size_t width, size_t height)
{
	glViewport(0, 0, width, height);
	m_width = width;
	m_height = height;
	setPerspectiveMatrix();
	glLoadIdentity();
}

void OpenGLWindow::setFullscreen(bool fullscreen) 
{
	DEVMODE dmScreenSettings = { 0 };

	dmScreenSettings.dmPelsWidth = m_width;
	dmScreenSettings.dmPelsHeight = m_height;
	dmScreenSettings.dmBitsPerPel = 32;
	dmScreenSettings.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;
	dmScreenSettings.dmSize = sizeof dmScreenSettings;

	if(fullscreen)
	{
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
	}
	else
	{
		ChangeDisplaySettings(&dmScreenSettings, CDS_RESET);
	}
}

bool OpenGLWindow::registerRenderable(Renderable* renderable)
{
	// if there is this renderable in vector return false
	
	for(Renderable* r : m_renderables)
	{
		if(r == renderable)
			return false;
	}
	
	//if(std::find(m_renderablesVec.begin(), m_renderablesVec.end(), renderable) != m_renderablesVec.end())
		//return false;

	std::cout << "registerRenderable before push_back" << std::endl;

	m_renderables.push_back(renderable);
	return true;
}

bool OpenGLWindow::unregisterRenderable(Renderable* renderable)
{
	auto it = std::find(m_renderables.begin(), m_renderables.end(), renderable);
	if(it == m_renderables.end())
		return false;
	m_renderables.erase(it);
	return true;
}

void OpenGLWindow::createContext()
{
	PIXELFORMATDESCRIPTOR pfd = { 0 };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;

	if(!(m_hDC = GetDC(m_hWnd))) {
		throw OpenGLWindowException("Nie mozna bylo utworzyc kontekstu renderowania");
	}

	int PixelFormat;

	if(!(PixelFormat = ChoosePixelFormat(m_hDC, &pfd))) {
		throw OpenGLWindowException("Blad wybierania formatu pikseli");
	}

	if(!(SetPixelFormat(m_hDC, PixelFormat, &pfd))) {
		throw OpenGLWindowException("Blad ustawiania formatu pikseli");
	}

	if(!(m_hRC = wglCreateContext(m_hDC))) {
		throw OpenGLWindowException("Blad tworzenia kontekstu OpenGL");
	}

	if(!wglMakeCurrent(m_hDC, m_hRC)) {
		throw OpenGLWindowException("Blad ustawiania aktualnego kontekstu OpenGL");
	}
}

void OpenGLWindow::setOrthoMatrix()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, m_width, m_height, 0.0, -3.0, 3.0);
	glMatrixMode(GL_MODELVIEW);
}

void OpenGLWindow::setPerspectiveMatrix()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, m_width/m_height, 1.0f, 1000.0f);
	glMatrixMode(GL_MODELVIEW);
}

void OpenGLWindow::initGL()
{
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
	m_defaultFontBase = CreateOutlineFont("Times New Roman", 10, 0.0f);
}

void OpenGLWindow::draw() 
{
	m_pCamera->updateCamera();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	m_mutex.lock();
	for(Renderable* renderable : m_renderables)
		renderable->render();
	m_mutex.unlock();
	SwapBuffers(m_hDC);
}

unsigned int OpenGLWindow::CreateOutlineFont(char *fontName, int fontSize, float depth)
{
	HFONT hFont;
	unsigned int base;

	base = glGenLists(256);

	hFont = CreateFont(
		fontSize, 0, 0, 0, FW_BOLD,     FALSE, FALSE, FALSE,
		ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,FF_DONTCARE | DEFAULT_PITCH,
		fontName
		);

	if(hFont == NULL)
		return 0;

	SelectObject(m_hDC, hFont);

	wglUseFontOutlines(
		m_hDC, 0, 255, base, 0.0f, depth, WGL_FONT_POLYGONS, NULL
		);

	return base;
}