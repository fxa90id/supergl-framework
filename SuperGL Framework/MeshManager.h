#ifndef __MESH_MANAGER_H__
#define __MESH_MANAGER_H__

#include <fstream>
#include <sstream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glext.h>
#include <unordered_map>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "MeshObject.h" 

using namespace std;

class MeshManager
{
public:
	static MeshManager* getSingleton();
	void add(string key, string path);
	void remove(string key);
	MeshObject* get(string key);
private:
	MeshManager() { }
	MeshManager(const MeshManager&);
	MeshManager& operator=(const MeshManager&);
	MeshObject * load_object(const char* filename);
	// GLuint createVBO(const void * data, int dataSize, GLenum target, GLenum usage);

	unordered_map<string, MeshObject*> m_container;
};

#endif
