#ifndef __MESH_OBJECT_H__
#define __MESH_OBJECT_H__

#include <Windows.h>
#include <cmath>
#include <gl/GLee.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include <vector>


#include "Renderable.h"

using namespace std;

class MeshObject : public Renderable
{
public:
	MeshObject();
	virtual ~MeshObject()
	{
		// if(m_vbo)
			//glDeleteBuffers(1, &m_vbo);
	}
	void attach(vector<glm::vec3>& vertices, vector<glm::vec3>& normals, vector<GLushort> elements)//GLuint vbo, GLuint elements)
	{
		m_vertices = vertices;
		m_normals = normals;
		m_elements = elements;
		// m_vbo = vbo;
		// m_elements = elements;
	}
	virtual void render();
	virtual Cube toCube() const;
protected:
	/* VBO */
	// GLuint m_vbo;
	// GLuint m_elements;
	vector<glm::vec3> m_vertices;
	vector<glm::vec3> m_normals;
	vector<GLushort> m_elements;
};

#endif