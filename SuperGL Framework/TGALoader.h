#ifndef __TGALOADER_H__
#define __TGALOADER_H__

#include "Texture.h"

bool LoadTGA(Texture * texture, char * filename);
bool LoadUncompressedTGA(Texture * texture, char * filename, FILE * fTGA);
bool LoadCompressedTGA(Texture * texture, char * filename, FILE * fTGA);

#endif