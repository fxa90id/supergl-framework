#ifndef __SPHERICALCAMERA_H__
#define __SPHERICALCAMERA_H__

#include <glm/glm.hpp>
#include "Camera.h"

class SphericalCamera : public Camera
{
public:
	SphericalCamera();
	SphericalCamera(glm::vec3 position, glm::vec3 target, glm::vec3 up, 
		float speed, float sensitivity, float verticalAngle, float horizontalAngle);
	virtual ~SphericalCamera() { }

	virtual void moveForward();
	virtual void moveBack();
	virtual void moveLeft();
	virtual void moveRight();
	virtual void moveUp();
	virtual void moveDown();

	virtual void lookLeft(float dx);
	virtual void lookRight(float dx);
	virtual void lookUp(float dy);
	virtual void lookDown(float dy);

	virtual void updateCamera();

	void setVerticalAngle(float angle) { m_verticalAngle = angle; }
	void setHorizontalAngle(float angle) { m_horizontalAngle = angle; }

	float getVerticalAngle() const { return m_verticalAngle; }
	float getHorizontalAngle() const { return m_horizontalAngle; }
private:
	void updateDirection();

	float m_verticalAngle;
	float m_horizontalAngle;
};

#endif