#ifndef __BULLET_H__
#define __BULLET_H__

#include <iostream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include <mutex>
#include "Renderable.h"
#include "Timer.h"


class Bullet : public Renderable
{
public:
	Bullet(glm::vec3 pos, glm::vec3 target, float radius, float speed);
	virtual ~Bullet();

	glm::vec3 getPosition() { return m_pos; }

	void move();
	virtual void render();
	virtual Cube toCube() const;
protected:
	glm::vec3 m_pos;
	glm::vec3 m_target;
	float m_radius;
	float m_speed;
	Timer m_timer;
	GLUquadricObj* m_quadric;
};
#endif