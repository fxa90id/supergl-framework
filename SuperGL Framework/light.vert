varying vec3 normal,halfVector;
varying vec4 lightDir;

void enableLightVert()
{
	 normal = normalize(gl_NormalMatrix * gl_Normal);
	 lightDir = normalize(gl_LightSource[0].position);
	 halfVector = normalize(gl_LightSource[0].halfVector.xyz);
}