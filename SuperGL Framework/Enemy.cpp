#include <iostream>
#include <ctime>
#include <sstream>
#include <gl/GLee.h>
#include "Enemy.h"
#include "TextureManager.h"
#include "ShaderManager.h"

Enemy::Enemy(glm::vec3* playerPos, glm::vec3 pos, int hp, float speed, float distance, float enemyRadius)
	:	m_playerPos(playerPos), m_pos(pos), m_hp(hp), m_speed(speed), m_enemyRadius(enemyRadius)
{
	m_radius = sqrt(pos.x * pos.x + pos.y * pos.y);
	m_angle = asin(pos.x / m_radius);
	m_distance = pos.z / distance;
	m_quadric = gluNewQuadric();
	gluQuadricDrawStyle(m_quadric, GLU_FILL);
	gluQuadricNormals(m_quadric, GLU_SMOOTH);
	gluQuadricOrientation(m_quadric, GLU_OUTSIDE);
	gluQuadricTexture(m_quadric, GL_TRUE);
	m_timer.start();
	srand(time(0));

	std::stringstream ss;
	ss << rand() % 10;
	textureName = ss.str();
}

void Enemy::move() 
{
	
	if(abs(m_playerPos->z - m_pos.z) >= 100)
	{
		m_angle += 0.05;
		m_pos.x = m_radius * sin(m_angle);
		m_pos.y = m_radius * cos(m_angle);
	} else {
	
		if(m_pos.x > m_playerPos->x)
			m_pos.x -= m_speed;
		else if(m_pos.x < m_playerPos->x)
			m_pos.x += m_speed;

		if(m_pos.y > m_playerPos->y)
			m_pos.y -= m_speed;
		else if(m_pos.y < m_playerPos->y)
			m_pos.y += m_speed;
	}

	if(m_pos.z < m_playerPos->z)
		m_pos.z -= m_distance * m_speed;
	else
		m_pos.z += m_distance * m_speed;

	//std::cout << "moving enemy to " << m_pos.x << " " << m_pos.y << " " << m_pos.z << std::endl;
}

void Enemy::render()
{
	if(m_timer.getTimer() >= 50)
	{
		move();
		m_timer.start();
	}

	glEnable( GL_TEXTURE_2D );

	glPushMatrix();
	glTranslatef(m_pos.x, m_pos.y, m_pos.z);
	GLuint location;
	location = glGetUniformLocation( ShaderManager::getSingleton()->getShaderProgram("simpleProgram"), "sampler");
	TextureManager::getSingletion()->enable(textureName);
	glUniform1i(location, 0);
	ShaderManager::getSingleton()->useProgram("simpleProgram");
	gluSphere(m_quadric,m_enemyRadius, 32, 32);
	ShaderManager::getSingleton()->disableProgram();
	glColor4f(1, 1, 1, 1);
	TextureManager::getSingletion()->disable();
	glPopMatrix();

	glDisable( GL_TEXTURE_2D );
}

Cube Enemy::toCube() const
{
	glm::vec3 min_point(m_pos.x - m_enemyRadius, m_pos.y - m_enemyRadius, m_pos.z - m_enemyRadius);
	glm::vec3 max_point(m_pos.x + m_enemyRadius, m_pos.y + m_enemyRadius, m_pos.z + m_enemyRadius);

	return Cube(min_point, max_point);
}