uniform sampler2D sampler;
void enableLightFrag();

void main()
{
	gl_FragColor = texture2D(sampler, gl_TexCoord[0].xy);
	enableLightFrag();
}