#ifndef __CUBE_H__
#define __CUBE_H__

#include <glm/glm.hpp>

class Cube
{
public:
	Cube(glm::vec3 min_point, glm::vec3 max_point)
		:	min_point(min_point), max_point(max_point)
	{ }

	bool overlap(Cube other);

	glm::vec3 min_point, max_point;
};

#endif