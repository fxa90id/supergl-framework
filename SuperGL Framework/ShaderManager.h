#pragma once

#include <GL/GL.h>
#include <GL/GLU.h>

#include <string>

#include "Shader.h"

using std::string;

class ShaderManager {
private:
	Shader * m_shaders;
	ShaderManager() {
		m_shaders = Shader::getSingleton();
	};
	~ShaderManager() {};
public:
	static ShaderManager * getSingleton() {
		static ShaderManager * p = new ShaderManager();
		return p;
	}
	bool loadShader(const string& path, const string& key, GLenum type);
	void createProgram(const string& programKey);
	void attachShader(const string& key, const string& programKey);
	void detachShader(const string& key, const string& programKey);
	void useProgram(const string& programKey);
	void disableProgram();
	void linkProgram(const string& programKey);
	void deleteShader(const string& key);
	void deleteShaderProgram(const string& programKey);
	unsigned int getShaderProgram(const string& programKey);
};