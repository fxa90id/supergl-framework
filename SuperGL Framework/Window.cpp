﻿#include <iostream>
#include <windowsx.h>
#include "Window.h"

using namespace SuperGLFramework;

Window::Window(std::string title, size_t width, size_t height, HINSTANCE instance)
	:	m_title(title), 
	m_width(width), m_height(height), 
	m_hInstance(instance), 
	m_hWnd(nullptr)
{
	if(m_hInstance == nullptr)
		m_hInstance = GetModuleHandle(0);
	
	m_className = title + "_class";
}

Window::~Window()
{
	destroy();
}

void Window::setTitle(std::string title)
{
	if(m_hWnd == nullptr) 
		throw WindowException("could not set title because window was not created");

	m_title = title;
	SetWindowText(m_hWnd, title.c_str());
}

void Window::setVisible(bool visible)
{
	if(m_hWnd == nullptr)
		throw WindowException("could not set window visible because window was not created");
		
	if(visible == true)
		ShowWindow(m_hWnd, SW_SHOW);
	else
		ShowWindow(m_hWnd, SW_HIDE);
}

void Window::resize(size_t width, size_t height)
{
	if(m_hWnd != nullptr)
	{
		RECT rec;
		if(GetWindowRect(m_hWnd, &rec))
		{
			if(MoveWindow(m_hWnd, rec.left, rec.top, width, height, TRUE) != 0)
			{
				m_width = width;
				m_height = height;
			}
		}
	}
}


void Window::create()
{
	registerWindowClass();
	RECT WindowRect = { 0 };
	WindowRect.right = m_width;
	WindowRect.bottom = m_height;

	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD dwStyle = WS_OVERLAPPEDWINDOW;
	AdjustWindowRectEx(&WindowRect, dwStyle, false, dwExStyle);

	m_hWnd = CreateWindowEx(
		dwExStyle, 
		m_className.c_str(),
		m_title.c_str(),
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle,
		0, 0,
		WindowRect.right - WindowRect.left,
		WindowRect.bottom - WindowRect.top,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		this);
}

void Window::destroy()
{
	if(m_hWnd != nullptr)
	{
		UnregisterClass(m_className.c_str(), m_hInstance);
		m_hWnd = nullptr;
	}
}

void Window::runEventLoop()
{
	if(m_hWnd == nullptr)
		throw WindowException("could not start event loop because window was not created");

	MSG msg;
	do 
	{
		if(PeekMessage(&msg, m_hWnd, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		for(auto it = loopFunc.begin(); it != loopFunc.end(); it++)
		{
			(*it)();
		}

	} while (m_hWnd != nullptr);
}

void Window::registerWindowClass()
{
	WNDCLASSEX wcex = { 0 };
	wcex.hInstance		= GetModuleHandle(0);
	wcex.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	wcex.hIconSm		= wcex.hIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.lpszClassName	= m_className.c_str();
	wcex.lpfnWndProc	= &Window::msgRouter;
	wcex.cbSize			= sizeof wcex;

	RegisterClassEx(&wcex);
}

LRESULT Window::msgRouter(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if(message == WM_NCCREATE) {
		CREATESTRUCT * pCreateStruct = reinterpret_cast<CREATESTRUCT*>(lParam);

		SetWindowLongPtr(handle, GWLP_USERDATA, 
			reinterpret_cast<long>(pCreateStruct->lpCreateParams));
	}

	Window* pWindow = reinterpret_cast<Window*>
		(GetWindowLongPtr(handle, GWLP_USERDATA));
	if(pWindow)
		pWindow->processEvent(message, wParam, lParam);
	return DefWindowProc(handle, message, wParam, lParam);
}

void Window::processEvent(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYDOWN:
		for(auto it = onKeyDownFunc.begin(); it != onKeyDownFunc.end(); it++)
		{
			(*it)(wParam);
		}			
		break;
	case WM_MOUSEMOVE:
		for(auto it = onMouseMoveFunc.begin(); it != onMouseMoveFunc.end(); it++)
		{
			(*it)(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		}	
		break;
	case WM_LBUTTONDOWN:
		if(wParam == MK_LBUTTON)
		{
			for(auto it = onMouseClickFunc.begin(); it != onMouseClickFunc.end(); it++)
			{
				(*it)(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			}	
		}
		break;
	case WM_SIZE:
		for(auto it = onResizeFunc.begin(); it != onResizeFunc.end(); it++)
		{
			(*it)(LOWORD(lParam), HIWORD(lParam));
		}	
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		destroy();
		break;
	default:
		break;
	}
}