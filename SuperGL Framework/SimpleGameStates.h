#ifndef __SIMPLEGAMESTATES_H__
#define __SIMPLEGAMESTATES_H__

enum class SimpleGameStates
{
	GAME_STARTED, GAME_PAUSED, GAME_ENDED
};
#endif