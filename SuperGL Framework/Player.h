#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <vector>
#include <glm/glm.hpp>
#include "Enemy.h"
#include "Bullet.h"

class Player
{
public:
	Player(glm::vec3 pos, glm::vec3 target, int hp) : m_pos(pos), m_target(target), m_hp(hp) { }
	virtual ~Player() { }

	void moveUp(float dy) { m_pos.y += dy; }
	void moveDown(float dy) { m_pos.y -= dy; }
	void moveLeft(float dx) { m_pos.x -= dx; }
	void moveRight(float dx) { m_pos.x += dx; }

	void setPos(glm::vec3 pos) { m_pos = pos; }
	void setTarget(glm::vec3 target) { m_target = target; }
	void setHp(int hp) { m_hp = hp; }
	
	glm::vec3& getPosition() { return m_pos; }
	glm::vec3 getTarget() const { return m_target; }
	int getHp() const { return m_hp; }

	Bullet* attack();
protected:
	glm::vec3 m_pos;
	glm::vec3 m_target;
	int m_hp;
	int m_attackPower;
};
#endif