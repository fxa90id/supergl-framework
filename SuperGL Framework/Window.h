#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <string>
#include <windows.h>
#include <exception>
#include <functional>
#include <vector>

namespace SuperGLFramework 
{
	class Window
	{
	public:
		Window(std::string title, size_t width = 800, size_t height = 600, HINSTANCE instance = nullptr);
		virtual ~Window();

		void setTitle(std::string title);		// sets title
		void setVisible(bool visible);			// sets window visible or hidden

		virtual void create();
		virtual void destroy();
		virtual void runEventLoop();
		virtual void resize(size_t width, size_t height);

		void addOnKeyDown(std::function<void(char)> func) { onKeyDownFunc.push_back(func); }
		void addOnMouseMove(std::function<void(int,int)> func) {  onMouseMoveFunc.push_back(func); }
		void addOnMouseClick(std::function<void(int,int)> func) {  onMouseClickFunc.push_back(func); }
		void addOnResize(std::function<void(int,int)> func) { onResizeFunc.push_back(func); }
		void addFunctionToLoop(std::function<void()> func) { loopFunc.push_back(func); }

		size_t getWidth() const { return m_width; }
		size_t getHeight() const { return m_height; }
		std::string getTitle() const;
		bool isAlive() const { return m_hWnd != nullptr; }

		HWND getHwnd() const { return m_hWnd; }

		class WindowException : public std::exception
		{
		public:
			WindowException(std::string msg) : m_msg(msg) { }
			virtual ~WindowException() { }

			virtual const char* what() const throw() { return m_msg.c_str(); }
		private:
			std::string m_msg;
		};
	protected:
		std::string m_title;
		size_t m_width;
		size_t m_height;
		HINSTANCE m_hInstance; // handle to current instance of application
		HWND m_hWnd;		  // handle to this window
	private:
		std::vector<std::function<void(char)>> onKeyDownFunc;
		std::vector<std::function<void(int,int)>> onMouseMoveFunc;
		std::vector<std::function<void(int,int)>> onMouseClickFunc;
		std::vector<std::function<void(int,int)>> onResizeFunc;
		std::vector<std::function<void()>> loopFunc;

		std::string m_className;
		
		void registerWindowClass();
		
		// routes event to correct Window object handler (processEvent)
		static LRESULT CALLBACK msgRouter(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

		// event handler
		void processEvent(UINT message, WPARAM wParam, LPARAM lParam);
	};
}

#endif