#include <iostream>
#include <gl/GLee.h>
#include "Bullet.h"
#include "TextureManager.h"
#include "ShaderManager.h"

Bullet::Bullet(glm::vec3 pos, glm::vec3 target, float radius, float speed)
	:	m_pos(pos), m_target(target), m_radius(radius), m_speed(speed)
{
	m_quadric = gluNewQuadric();
	gluQuadricDrawStyle(m_quadric, GLU_FILL);
	gluQuadricNormals(m_quadric, GLU_SMOOTH);
	gluQuadricOrientation(m_quadric, GLU_OUTSIDE);
	gluQuadricTexture(m_quadric, GL_TRUE);
	m_timer.start();
}

Bullet::~Bullet()
{
}

void Bullet::move()
{
	m_pos += m_target*m_speed;
	//std::cout << "moving bullet to: " << m_pos.x << " " << m_pos.y << " " << m_pos.z << std::endl;
}

void Bullet::render()
{
	if(m_timer.getTimer() >= 30)
	{
		move();
		m_timer.start();
	}

	glEnable( GL_TEXTURE_2D );

	glPushMatrix();
	glTranslatef(m_pos.x, m_pos.y, m_pos.z);

	GLuint location;
	location = glGetUniformLocation( ShaderManager::getSingleton()->getShaderProgram("simpleProgram"), "sampler");
	TextureManager::getSingletion()->enable("ice");
	glUniform1i(location, 0);
	ShaderManager::getSingleton()->useProgram("simpleProgram");
	gluSphere(m_quadric, m_radius, 32, 32);
	ShaderManager::getSingleton()->disableProgram();
	glColor4f(1, 1, 1, 1);
	TextureManager::getSingletion()->disable();
	glPopMatrix();

	glDisable( GL_TEXTURE_2D );

}

Cube Bullet::toCube() const
{
	glm::vec3 min_point(m_pos.x - m_radius, m_pos.y - m_radius, m_pos.z - m_radius);
	glm::vec3 max_point(m_pos.x + m_radius, m_pos.y + m_radius, m_pos.z + m_radius);

	return Cube(min_point, max_point);
}