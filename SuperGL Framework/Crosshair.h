#ifndef __CROSSHAIR_H__
#define __CROSSHAIR_H__

#include "Renderable.h"

class Crosshair : public Renderable
{
public:
	Crosshair(glm::vec4 color) : m_color(color) { }
	virtual ~Crosshair() { }

	virtual void render();
	virtual Cube toCube() const;
private:
	glm::vec4 m_color;
};
#endif