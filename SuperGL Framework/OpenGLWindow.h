#ifndef __OPENGLWINDOW_H__
#define __OPENGLWINDOW_H__

#include <memory>
#include <thread>
#include <map>
#include <string>
#include <list>
#include <mutex>

#include "Window.h"
#include "Camera.h"
#include "Renderable.h"

#pragma comment(lib, "opengl32")
#pragma comment(lib, "glu32")

namespace SuperGLFramework
{
	class OpenGLWindow : public Window
	{
	public:
		OpenGLWindow(std::string title, size_t width = 800, size_t height = 600, HINSTANCE instance = nullptr);
		virtual ~OpenGLWindow();

		virtual void create();
		virtual void destroy();
		virtual void runEventLoop();
		virtual void resize(size_t width, size_t height);

		void setFullscreen(bool fullscreen);
		bool registerRenderable(Renderable* renderable);
		bool unregisterRenderable(Renderable* renderable);

		unsigned int CreateOutlineFont(char *fontName, int fontSize, float depth);

		void setDefaultFontBase(unsigned int fontBase) { m_defaultFontBase = fontBase; }
		unsigned int getDefaultFontBase() const { return m_defaultFontBase; }
		
		Camera* getCamera() { return m_pCamera; }
		std::mutex& getMutex() { return m_mutex; }

		class OpenGLWindowException : public WindowException
		{
		public:
			OpenGLWindowException(std::string msg) : WindowException(msg) { }
			virtual ~OpenGLWindowException() { }
		};

		Camera* m_pCamera;
	protected:
		HGLRC m_hRC;
		HDC m_hDC;

	private:
		std::list<Renderable*> m_renderables;
		unsigned int m_defaultFontBase;
		std::mutex m_mutex;

		void createContext();
		void setOrthoMatrix();
		void setPerspectiveMatrix();
		void initGL();
		void draw();
	};
}
#endif